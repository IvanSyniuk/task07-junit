package сom.controller.Impl;

import сom.controller.SubController;
import сom.model.SubSequence;

import java.util.regex.Matcher;

public class SubControllerImpl implements SubController {
    private SubSequence subSequence;

    @Override
    public void showLongestSub() {

    }

    public String showSubSettings(int arr[], char[] chars, Matcher m) {
        int firstIndex = 0;
        int maxLen = 0;
        int lastIndex = 0;
        while (m.find()) {
            String sub = m.group();
            if ((chars[m.start() - 1] < chars[m.start()] && chars[m.end() - 1] > chars[m.end()])) {
                if (sub.length() > maxLen) {
                    firstIndex = m.start();
                    lastIndex = m.end() - 1;
                    maxLen = sub.length();
                }
            }
        }
        return "firstIndex = " + firstIndex + "\nlastIndex = " + lastIndex + "\nmaxLenght = " + maxLen +
                "\nmaxSubSequence = " + new SubSequence().IntArrayToString(arr).substring(firstIndex, lastIndex + 1);
    }
}
