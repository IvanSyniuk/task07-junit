package сom.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import сom.controller.Impl.SubControllerImpl;
import сom.model.SubSequence;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class View {
    private static Logger logger = LogManager.getLogger(View.class);
    SubSequence subSequence = new SubSequence();
    SubControllerImpl subController = new SubControllerImpl();
    int[] arr = {1, 3, 3, 3, 3, 1, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3};

    String text = subSequence.IntArrayToString(arr);
    Matcher m = Pattern.compile("(.)\\1+").matcher(text);
    char[] chars = text.toCharArray();


    public void show() {
        logger.info(subController.showSubSettings(arr, chars, m));
        logger.info("array =  " + text);
    }
}
