package сom.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import сom.controller.Impl.SubControllerImpl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class SubControllerImplTest {
    @InjectMocks
    private SubControllerImpl subController;

    int[] actualIntArray = {1, 3, 3, 3, 3, 1, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3};
    String text = "1333312233333333434444444443";
    Matcher m = Pattern.compile("(.)\\1+").matcher(text);
    char[] actualCharArray = text.toCharArray();


    @Test
    void showSubSequence_ReturnCorrectSettings() {
        String testString = subController.showSubSettings(actualIntArray, actualCharArray, m);
        String actualString = "firstIndex = " + 18 + "\nlastIndex = " + 26 + "\nmaxLenght = " + 9 +
                "\nmaxSubSequence = " + 444444444;
        assertEquals(testString, actualString);
    }

    @Test
    void showSubSequence_ReturnNotNull() {
        String testString = subController.showSubSettings(actualIntArray, actualCharArray, m);
        assertNotNull(testString);
    }
}