package сom.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class SubSequenceTest {
    @InjectMocks
    SubSequence subSequence;
    int[] arr = {1, 3, 3, 3, 3, 1, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3};

    @Test
    void intArrayToString_ReturnNotNull() {
        String subTest = subSequence.IntArrayToString(arr);
        assertNotNull(subTest);
    }

    @Test
    void intArrayToString_ReturnCorrectString() {
        String testString = subSequence.IntArrayToString(arr);
        String actualString = "1333312233333333434444444443";
        assertEquals(testString, actualString);
    }
}
